package com.example.juhoh.exceptions;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class TheaterList {
    private ArrayList<Theater> theaters = new ArrayList<Theater>();
    private ArrayList<String> movies = new ArrayList<>();

    private static final TheaterList instance = new TheaterList();

    private TheaterList () {

    }

    public static TheaterList getInstance () {
        return instance;
    }

    public ArrayList<Theater> getTheaterList () {
        theaters.clear();
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String urlString = "https://www.finnkino.fi/xml/TheatreAreas/";
            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getDocumentElement().getElementsByTagName("TheatreArea");

            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                Element element = (Element) node;
                String string = element.getElementsByTagName("Name").item(0).getTextContent();
                if (Character.isUpperCase(string.charAt(string.length() - 1))) {
                    Theater theater = new Theater();
                    theater.setName(element.getElementsByTagName("Name").item(0).getTextContent());
                    theater.setId(element.getElementsByTagName("ID").item(0).getTextContent());
                    theaters.add(theater);
                }
                else {
                    continue;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            System.out.println("###DONE###");
        }
        return theaters;
    }

    public ArrayList<String> getMovieList (String name, String date, String start, String end) {
        movies.clear();
        String id = "0000";
        for (Theater theater:theaters) {
            if (name.equals(theater.getName())) {
                id = theater.getId();
                break;
            }
        }
        String urlString = "http://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + date;

        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getDocumentElement().getElementsByTagName("Show");

            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                Element element = (Element) node;
                String showStart = element.getElementsByTagName("dttmShowStart").item(0).getTextContent();
                showStart = showStart.substring(11);
                showStart = showStart.substring(0, showStart.length() - 3);
                int hours = Integer.parseInt(showStart.substring(0, showStart.length() - 3));
                int minutes = Integer.parseInt(showStart.substring(3));
                //if (Integer.parseInt(start.substring(0, start.length() - 3)>hours)) //TODO
                movies.add(element.getElementsByTagName("Title").item(0).getTextContent());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            System.out.println("###DONE###");
        }
        return movies;
    }
}
