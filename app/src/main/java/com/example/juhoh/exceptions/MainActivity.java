package com.example.juhoh.exceptions;

import android.app.DatePickerDialog;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private String date = "";
    private String currentItem;
    private String start = "00:00";
    private String end = "23:59";

    private TextView displayDate;
    private TextView dash;
    private TextView displayStart;
    private TextView displayEnd;
    private TextView displayMovies;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private Spinner selectTheater;
    private Spinner selectFilm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        displayDate = (TextView) findViewById(R.id.displayDate);
        dash = (TextView) findViewById(R.id.dash);
        displayStart = (TextView) findViewById(R.id.displayStart);
        displayEnd = (TextView) findViewById(R.id.displayEnd);
        displayMovies = (TextView) findViewById(R.id.displayMovies);
        selectTheater = (Spinner) findViewById(R.id.selectTheater);
        selectFilm = (Spinner) findViewById(R.id.selectFilm);
        theaterSelector();
        itemSelected();
    }

    private void theaterSelector() {
        TheaterList tl = TheaterList.getInstance();
        ArrayList<Theater> theaters = tl.getTheaterList();
        ArrayAdapter<Theater> adapter = new ArrayAdapter<Theater>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, theaters);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectTheater.setAdapter(adapter);
    }

    private void updateList() {
        TheaterList tl = TheaterList.getInstance();
        ArrayList<String> movies = tl.getMovieList(currentItem, date, start, end);
        displayMovies.setText("");
        for (String movie:movies) {
            displayMovies.append(movie + "\n");
        }
    }

    private void itemSelected() {
        selectTheater.setOnItemSelectedListener(
            new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    Object item = parent.getItemAtPosition(pos);
                    currentItem = item.toString();
                    if (date.equals("")) {
                        String pattern = "dd.mm.yyyy";
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                        date = simpleDateFormat.format(new Date());
                    }
                    updateList();
                }
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
    }

    public void changeDate(View v) {
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);
        DatePickerDialog dpd = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                date = mDay + "." + (mMonth + 1) + "." + mYear;
                displayDate.setText(date);
            }
        }, day, month, year);
        dpd.show();
        updateList();
    }

    public void changeStart(View v) {

    }

    public void changeEnd(View v) {

    }
}
